#ifndef FILEHANDLER_LIBRARY_H
#define FILEHANDLER_LIBRARY_H

#include <string>
#include <vector>
#include <fstream>


template <typename T>
class File
{
private:
    std::string m_filePath;
    std::vector<T> m_fileContent;
    std::ifstream m_inputFile;
    std::ofstream m_outputFile;


public:

    File(std::string Path);
    ~File();
    std::vector<T> getFileContent() const;
    void save(std::vector<T> &newContent);

};





//Implementation section





template<typename T>
File<T>::File(std::string Path) : m_filePath(std::move(Path)){

    T temp;
    m_inputFile.open(m_filePath);

    if(m_inputFile.is_open())
    {
        while(!m_inputFile.eof())
        {
            m_inputFile >> temp;

            m_fileContent.push_back(temp);

        }
        m_inputFile.close();
    }
    m_fileContent.pop_back();

}


template<typename T>
std::vector<T> File<T>::getFileContent() const{
    return m_fileContent;
}

template<typename T>
void File<T>::save(std::vector<T> &newContent) {

    m_outputFile.open(m_filePath + " rozwiazanie");

    for(auto& x: newContent)
    {
        m_outputFile << x << std::endl;
    }

    m_outputFile.close();
}


template<typename T>
File<T>::~File() {

    if(m_inputFile.is_open())
    {
        m_inputFile.close();
    }
    if(m_outputFile.is_open())
    {
        m_outputFile.close();
    }
}


#endif